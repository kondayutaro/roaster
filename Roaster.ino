//www.elegoo.com
//2016.12.9

#include <SPI.h>
#include "Adafruit_MAX31855.h"
// Example creating a thermocouple instance with software SPI on any three
// digital IO pins.
#define MAXDO_1   3
#define MAXCS_1   4
#define MAXCLK_1  5
// initialize the Thermocouple
Adafruit_MAX31855 thermocouple_1(MAXCLK_1, MAXCS_1, MAXDO_1);

void setup()
{
  Serial.begin(9600);
}

void loop()
{
  double eT = thermocouple_1.readCelsius();

  Serial.print(eT);
  Serial.print(",");
}
